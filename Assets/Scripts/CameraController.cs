using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /*
     * based on https://www.youtube.com/watch?v=zVX9-c_aZVg
     * https://www.youtube.com/watch?v=qsxRQIhgNmk
     */

    [SerializeField]
    [Tooltip("What object should the camera be looking at")]
    private Transform target; // sol

    public  Transform panoramic;

    [SerializeField]
    [Tooltip("How offset will the camera be to the target")]
    private Vector3 offset = new Vector3(0, 3, -6);

    [SerializeField]
    [Tooltip("Mouse sensitivity")]
    private float mouseSensitivity = 0.5f;


    private bool canLookArround = true;

    // maybe will use who knows
    private bool followPlanet = false;

    void Start()
    {

        target = GameController.celestials[GameController.Celestial.Sun].model.transform;
        transform.LookAt(target);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        // If the mouse is held down (or the screen is pressed on mobile)
        if (Input.GetMouseButton(0))
        {
            LookArround(Input.mousePosition);
        }
    }

    public void ChangeTarget(GameController.Celestial cel)
    {
        target = GameController.celestials[cel].model.transform;

        // Set position to an offset of target
        transform.position = target.position + offset;

        // Change rotation to face the target
        transform.LookAt(target);transform.position = target.position + offset;

        // Change rotation to face the target
        transform.LookAt(target);
    }

    public void ResetToPanoramic()
    {
        target = panoramic;
        transform.position = target.position + new Vector3(0, 50, -200);
        transform.LookAt(target);
    }

    private void LookArround(Vector3 pos)
    {
        if (canLookArround)
        {
            // Convert to viewport (0 to 1 scale)
            var targetPos = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, 10f));

            // Determine target rotation
            var targetRot = Quaternion.LookRotation(targetPos - transform.position);

            // Change it
            // transform.rotation = targetRot;

            // Slerp
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, mouseSensitivity * Time.deltaTime);

            /*
            // Directions
            var mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
            var mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

            // Update rotation angles
            rotationX += mouseX;
            rotationY += mouseY;

            // Clamp
            rotationX = Mathf.Clamp(rotationX, -40, 40);

            // Rotate by Euler
            transform.localEulerAngles = new Vector3(rotationX, rotationY, 0);

            */
        }

    }
    public void SetLookArround(bool can)
    {
        canLookArround = can;
    }
}