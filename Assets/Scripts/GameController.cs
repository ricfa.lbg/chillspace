using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    // data about the celestial objects
    public static Dictionary<Celestial, CelestialData> celestials = new Dictionary<Celestial, CelestialData>();

    // using [SerializeField] because if you make it public you need to be careful cause the inspector overrides these values
    [SerializeField]
    private float bigPlanetScaleMultiplier = 0.008f;

    public enum Celestial
    {
        Sun,
        Mercury,
        Venus,
        Earth,
        Mars,
        Jupiter,
        Saturn,
        Uranus,
        Neptune,
        Moon,
        Phobos,
        Deimos,
        SaturnRings
    };

    // realistic mode - uses true distances and sizes (in the units used in CelestialData)
    bool realisticMode = false;


    // Start is called before the first frame update
    void Start()
    {

        foreach (Celestial name in Enum.GetValues(typeof(Celestial)))
        {
            var str = name.ToString();
            var model = GameObject.Find(str);
            var im = Resources.Load<Sprite>(str);
            Debug.Log(Resources.Load<Sprite>(str));
            var celestObj = new CelestialData(model, im);
            celestials.Add(name, celestObj);
        }

        var sun = celestials[Celestial.Sun];
        sun.engName = "Sun";
        sun.ptName = "Sol";
        sun.mass = 1989000;
        sun.modelMass = 0;
        sun.SetDiameter(1392700);
        sun.modelRadius = 696340f;
        sun.density = 0;
        sun.gravity = 0;
        sun.escapeVelocity = 0;
        sun.rotationPeriod = 0;
        sun.modelRotationPeriod = 0;
        sun.lengthOfDay = 0;
        sun.modelLengthOfDay = 0;
        sun.periphelion = 0;
        sun.aphelion = 0;
        sun.distanceFromSun = 0;
        sun.modelDistanceFromSun = 0;
        sun.orbitalPeriod = 0;
        sun.modelOrbitalPeriod = 0;
        sun.orbitalVelocity = 0;
        sun.orbitalInclination = 0;
        sun.orbitalEccentricity = 0;
        sun.obliquityToOrbit = 0;
        sun.meanTemperature = 0;
        sun.surfacePressure = 0;
        sun.numberOfMoons = 0;
        sun.hasRingSystem = false;
        sun.hasMagneticField = false;
        sun.moreInfo = "";

        var mercury = celestials[Celestial.Mercury];
        mercury.engName = "Mercury";
        mercury.ptName = "Merc�rio";
        mercury.mass = 0.330f;
        mercury.modelMass = 0;
        mercury.SetDiameter(4879);
        mercury.modelRadius = 2440;
        mercury.density = 5429;
        mercury.gravity = 3.7f;
        mercury.escapeVelocity = 4.3f;
        mercury.rotationPeriod = 1407.6f;
        mercury.modelRotationPeriod = 1407.6f; // 58
        mercury.lengthOfDay = 4222.6f;
        mercury.modelLengthOfDay = 0;
        mercury.periphelion = 46.0f;
        mercury.aphelion = 69.8f;
        mercury.distanceFromSun = 57.9f;
        mercury.modelDistanceFromSun = 57.9f;
        mercury.orbitalPeriod = 88;
        mercury.modelOrbitalPeriod = 88;
        mercury.orbitalVelocity = 47.4f;
        mercury.orbitalInclination = 7.0f;
        mercury.orbitalEccentricity = 0.206f;
        mercury.obliquityToOrbit = 0.034f;
        mercury.meanTemperature = 167;
        mercury.surfacePressure = 0;
        mercury.numberOfMoons = 0;
        mercury.hasRingSystem = false;
        mercury.hasMagneticField = true;
        mercury.moreInfo = "";

        var venus = celestials[Celestial.Venus];
        venus.engName = "Venus";
        venus.ptName = "V�nus";
        venus.mass = 4.87f;
        venus.modelMass = 0;
        venus.SetDiameter(12104);
        venus.modelRadius = 6052;
        venus.density = 5243;
        venus.gravity = 8.9f;
        venus.escapeVelocity = 10.4f;
        venus.rotationPeriod = -5832.5f;
        venus.modelRotationPeriod = -5832.5f; // -243
        venus.lengthOfDay = 2802;
        venus.modelLengthOfDay = 0;
        venus.periphelion = 107.5f;
        venus.aphelion = 108.9f;
        venus.distanceFromSun = 108.2f;
        venus.modelDistanceFromSun = 108.2f;
        venus.orbitalPeriod = 224.7f;
        venus.modelOrbitalPeriod = 224.7f;
        venus.orbitalVelocity = 35;
        venus.orbitalInclination = 3.4f;
        venus.orbitalEccentricity = 0.007f;
        venus.obliquityToOrbit = 177.4f;
        venus.meanTemperature = 464;
        venus.surfacePressure = 92;
        venus.numberOfMoons = 0;
        venus.hasRingSystem = false;
        venus.hasMagneticField = false;
        venus.moreInfo = "";


        var earth = celestials[Celestial.Earth];
        earth.engName = "Earth";
        earth.ptName = "Terra";
        earth.mass = 5.97f;
        earth.modelMass = 0;
        earth.SetDiameter(12756);
        earth.modelRadius = 6378;
        earth.density = 5514;
        earth.gravity = 9.8f;
        earth.escapeVelocity = 11.2f;
        earth.rotationPeriod = 23.9f;
        earth.modelRotationPeriod = 23.9f; // 1
        earth.lengthOfDay = 24;
        earth.modelLengthOfDay = 0;
        earth.periphelion = 147.1f;
        earth.aphelion = 152.1f;
        earth.distanceFromSun = 149.6f;
        earth.modelDistanceFromSun = 149.6f;
        earth.orbitalPeriod = 365.2f;
        earth.modelOrbitalPeriod = 365.2f;
        earth.orbitalVelocity = 29.8f;
        earth.orbitalInclination = 0;
        earth.orbitalEccentricity = 0.017f;
        earth.obliquityToOrbit = 23.4f;
        earth.meanTemperature = 15;
        earth.surfacePressure = 1;
        earth.numberOfMoons = 1;
        earth.hasRingSystem = false;
        earth.hasMagneticField = true;
        earth.moreInfo = "";

        var moon = celestials[Celestial.Moon];
        moon.engName = "Moon";
        moon.ptName = "Lua";
        moon.mass = 0.073f;
        moon.modelMass = 0;
        moon.SetDiameter(3475);
        moon.modelRadius = 1737.5f; // 500
        moon.density = 3340;
        moon.gravity = 1.6f;
        moon.escapeVelocity = 2.4f;
        moon.rotationPeriod = 655.7f;
        moon.modelRotationPeriod = 655.7f; // 1
        moon.lengthOfDay = 708.7f;
        moon.modelLengthOfDay = 0;
        moon.periphelion = 0.363f;
        moon.aphelion = 0.406f;
        moon.distanceFromSun = 0.348f;
        moon.modelDistanceFromSun = 5f; // 5
        moon.orbitalPeriod = 27.3f;
        moon.modelOrbitalPeriod = 27.3f; // 14
        moon.orbitalVelocity = 1;
        moon.orbitalInclination = 5.1f;
        moon.orbitalEccentricity = 0.055f;
        moon.obliquityToOrbit = 6.7f;
        moon.meanTemperature = -20;
        moon.surfacePressure = 0;
        moon.numberOfMoons = 0;
        moon.hasRingSystem = false;
        moon.hasMagneticField = false;
        moon.moreInfo = "";

        var mars = celestials[Celestial.Mars];
        mars.engName = "Mars";
        mars.ptName = "Marte";
        mars.mass = 0.642f;
        mars.modelMass = 0;
        mars.SetDiameter(6792);
        mars.modelRadius = 3396;
        mars.density = 3934;
        mars.gravity = 3.7f;
        mars.escapeVelocity = 5.0f;
        mars.rotationPeriod = 24.6f;
        mars.modelRotationPeriod = 24.6f; // 1
        mars.lengthOfDay = 24.7f;
        mars.modelLengthOfDay = 0;
        mars.periphelion = 206.7f;
        mars.aphelion = 249.3f;
        mars.distanceFromSun = 228;
        mars.modelDistanceFromSun = 228;
        mars.orbitalPeriod = 687;
        mars.modelOrbitalPeriod = 687;
        mars.orbitalVelocity = 24.1f;
        mars.orbitalInclination = 1.8f;
        mars.orbitalEccentricity = 0.094f;
        mars.obliquityToOrbit = 25.2f;
        mars.meanTemperature = -65;
        mars.surfacePressure = 0.01f;
        mars.numberOfMoons = 2;
        mars.hasRingSystem = false;
        mars.hasMagneticField = false;
        mars.moreInfo = "";


        var jupiter = celestials[Celestial.Jupiter];
        jupiter.engName = "Jupiter";
        jupiter.ptName = "Jupiter";
        jupiter.mass = 1898;
        jupiter.modelMass = 0;
        jupiter.SetDiameter(142984);
        jupiter.modelRadius = 71492 * bigPlanetScaleMultiplier; // 4000
        jupiter.density = 1326;
        jupiter.gravity = 23.1f;
        jupiter.escapeVelocity = 59.5f;
        jupiter.rotationPeriod = 9.9f;
        jupiter.modelRotationPeriod = 9.9f; // .42
        jupiter.lengthOfDay = 9.9f;
        jupiter.modelLengthOfDay = 0;
        jupiter.periphelion = 740.6f;
        jupiter.aphelion = 816.4f;
        jupiter.distanceFromSun = 778.5f;
        jupiter.modelDistanceFromSun = 778.5f; // 400
        jupiter.orbitalPeriod = 4331;
        jupiter.modelOrbitalPeriod = 4331; // 4333
        jupiter.orbitalVelocity = 13.1f;
        jupiter.orbitalInclination = 1.3f;
        jupiter.orbitalEccentricity = 0.049f;
        jupiter.obliquityToOrbit = 3.1f;
        jupiter.meanTemperature = -110;
        jupiter.surfacePressure = 0; // unknown
        jupiter.numberOfMoons = 79;
        jupiter.hasRingSystem = true;
        jupiter.hasMagneticField = true;
        jupiter.moreInfo = "";

        var saturn = celestials[Celestial.Saturn];
        saturn.engName = "Saturn";
        saturn.ptName = "Saturno";
        saturn.mass = 568;
        saturn.modelMass = 0;
        saturn.SetDiameter(120536);
        saturn.modelRadius = 60268 * bigPlanetScaleMultiplier; // 5000
        saturn.density = 687;
        saturn.gravity = 9.0f;
        saturn.escapeVelocity = 35.5f;
        saturn.rotationPeriod = 10.7f;
        saturn.modelRotationPeriod = 10.7f; // .44
        saturn.lengthOfDay = 10.7f;
        saturn.modelLengthOfDay = 0;
        saturn.periphelion = 1357.6f;
        saturn.aphelion = 1506.5f;
        saturn.distanceFromSun = 1432;
        saturn.modelDistanceFromSun = 1432; // 500
        saturn.orbitalPeriod = 10747;
        saturn.modelOrbitalPeriod = 10747; // 10759
        saturn.orbitalVelocity = 9.7f;
        saturn.orbitalInclination = 2.5f;
        saturn.orbitalEccentricity = 0.052f;
        saturn.obliquityToOrbit = 26.7f;
        saturn.meanTemperature = -140;
        saturn.surfacePressure = 0; // unknown
        saturn.numberOfMoons = 82;
        saturn.hasRingSystem = true;
        saturn.hasMagneticField = true;
        saturn.moreInfo = "";

        var uranus = celestials[Celestial.Uranus];
        uranus.engName = "Uranus";
        uranus.ptName = "Ur�no";
        uranus.mass = 86.8f;
        uranus.modelMass = 0;
        uranus.SetDiameter(51118);
        uranus.modelRadius = 25559f * bigPlanetScaleMultiplier; // 6000
        uranus.density = 1270;
        uranus.gravity = 8.7f;
        uranus.escapeVelocity = 21.3f;
        uranus.rotationPeriod = -17.2f;
        uranus.modelRotationPeriod = -17.2f; // -0.7
        uranus.lengthOfDay = 17.2f;
        uranus.modelLengthOfDay = 0;
        uranus.periphelion = 2732.7f;
        uranus.aphelion = 3001.4f;
        uranus.distanceFromSun = 2867;
        uranus.modelDistanceFromSun = 2867; // 600
        uranus.orbitalPeriod = 30589;
        uranus.modelOrbitalPeriod = 30589; // 30689
        uranus.orbitalVelocity = 6.8f;
        uranus.orbitalInclination = 0.8f;
        uranus.orbitalEccentricity = 0.047f;
        uranus.obliquityToOrbit = 97.8f;
        uranus.meanTemperature = -195;
        uranus.surfacePressure = 0; // unknown
        uranus.numberOfMoons = 27;
        uranus.hasRingSystem = true;
        uranus.hasMagneticField = true;
        uranus.moreInfo = "";

        var neptune = celestials[Celestial.Neptune];
        neptune.engName = "Neptune";
        neptune.ptName = "Neptune";
        neptune.mass = 102;
        neptune.modelMass = 0;
        neptune.SetDiameter(49528);
        neptune.modelRadius = 24764 * bigPlanetScaleMultiplier; // 4000
        neptune.density = 1638;
        neptune.gravity = 11;
        neptune.escapeVelocity = 23.5f;
        neptune.rotationPeriod = 16.1f;
        neptune.modelRotationPeriod = 16.1f; // 0.67
        neptune.lengthOfDay = 16.1f;
        neptune.modelLengthOfDay = 0;
        neptune.periphelion = 4471.1f;
        neptune.aphelion = 4558.9f;
        neptune.distanceFromSun = 4515.0f;
        neptune.modelDistanceFromSun = 4515.0f; // 700
        neptune.orbitalPeriod = 59800;
        neptune.modelOrbitalPeriod = 59800; // 60195
        neptune.orbitalVelocity = 5.4f;
        neptune.orbitalInclination = 1.8f;
        neptune.orbitalEccentricity = 0.010f;
        neptune.obliquityToOrbit = 28.3f;
        neptune.meanTemperature = -200;
        neptune.surfacePressure = 0; // unknown
        neptune.numberOfMoons = 14;
        neptune.hasRingSystem = true;
        neptune.hasMagneticField = true;
        neptune.moreInfo = "";

        var phobos = celestials[Celestial.Phobos];
        phobos.engName = "Phobos";
        phobos.ptName = "Fobos";
        phobos.mass = 10.6f * 10e15f/ 10e24f;
        phobos.modelMass = 0;
        phobos.SetDiameter(22.533f);
        phobos.modelRadius = 150; // 150
        phobos.density = 0;
        phobos.gravity = 0;
        phobos.escapeVelocity = 0;
        phobos.rotationPeriod = uranus.rotationPeriod;
        phobos.modelRotationPeriod = uranus.modelRotationPeriod; // 1
        phobos.lengthOfDay = 0;
        phobos.modelLengthOfDay = 0;
        phobos.periphelion = 0;
        phobos.aphelion = 0;
        phobos.distanceFromSun = 6000f/10e6f;
        phobos.modelDistanceFromSun = 10; // 10
        phobos.orbitalPeriod = 7.63f;
        phobos.modelOrbitalPeriod = 7.63f; // 54
        phobos.orbitalVelocity = 0;
        phobos.orbitalInclination = 0;
        phobos.orbitalEccentricity = 0;
        phobos.obliquityToOrbit = 0;
        phobos.meanTemperature = 0;
        phobos.surfacePressure = 0;
        phobos.numberOfMoons = 0;
        phobos.hasRingSystem = false;
        phobos.hasMagneticField = false;
        phobos.moreInfo = "";

        var deimos = celestials[Celestial.Deimos];
        deimos.engName = "Deimos";
        deimos.ptName = "Deimos";
        deimos.mass = 1.47f * 10e15f / 10e24f; ;
        deimos.modelMass = 0;
        deimos.SetDiameter(12.4f);
        deimos.modelRadius = 250; // 250
        deimos.density = 0;
        deimos.gravity = 0;
        deimos.escapeVelocity = 0;
        deimos.rotationPeriod = uranus.rotationPeriod;
        deimos.modelRotationPeriod = uranus.modelRotationPeriod; // 1
        deimos.lengthOfDay = 0;
        deimos.modelLengthOfDay = 0;
        deimos.periphelion = 0;
        deimos.aphelion = 0;
        deimos.distanceFromSun = 0;
        deimos.modelDistanceFromSun = 7; // 7
        deimos.orbitalPeriod = 30.3f;
        deimos.modelOrbitalPeriod = 30.3f;
        deimos.orbitalVelocity = 0;
        deimos.orbitalInclination = 0;
        deimos.orbitalEccentricity = 0;
        deimos.obliquityToOrbit = 0;
        deimos.meanTemperature = 0;
        deimos.surfacePressure = 0;
        deimos.numberOfMoons = 0;
        deimos.hasRingSystem = false;
        deimos.hasMagneticField = false;
        deimos.moreInfo = "";

        var saturnRings = celestials[Celestial.SaturnRings];
        saturnRings.engName = "Saturn Rings";
        saturnRings.ptName = "Aneis de Saturno";
        saturnRings.SetDiameter(2.35f*saturn.diameter); // 2.35f*saturn.diameter
        saturnRings.modelRadius = 4f * 250; // 2.35f * 250
        saturnRings.modelDistanceFromSun = saturnRings.modelRadius;
        saturnRings.obliquityToOrbit = 26.7f;


        // too lazy for pluto...
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

/// <summary>
/// compiles all the relevant data about each object.
/// It also points to the model used.
/// Model parameters are different from the true values.
/// Data taken from https://nssdc.gsfc.nasa.gov/planetary/factsheet/
/// </summary>
public class CelestialData
{
    public GameObject model;
    public Sprite celestialImage;

    public string engName;
    public string ptName;

    public float mass; // times 10^24kg
    public float modelMass; // <---- not used

    public float diameter; // km
    public float radius; // km ---> prof chamou raio
    public float modelRadius;

    public float density; // kg/m^3
    public float gravity; // m/s^2
    public float escapeVelocity; // km/s

    // how long it takes to make a full revolution on it's axis (aka a day)
    public float rotationPeriod; // hours --> prof chamou rotacao
    public float modelRotationPeriod;
    public float lengthOfDay; // hours
    public float modelLengthOfDay; // <---- not used

    public float periphelion; // 10^6 km
    public float aphelion; // 10^6 km

    // how far away from sun (or the planet in case of a satellite)
    public float distanceFromSun; // 10^6 km  ---> prof chamou distancia_sol
    public float modelDistanceFromSun;

    // how long it takes to make a full path arround the sun (or the planet in case of a satellite) (aka a year)
    public float orbitalPeriod; // days ---> prof chamou orbita
    public float modelOrbitalPeriod;

    public float orbitalVelocity; // km/s
    public float orbitalInclination; // degrees
    public float orbitalEccentricity;
    public float obliquityToOrbit; // degrees

    public float meanTemperature; // �C
    public float surfacePressure; // bars
    public int numberOfMoons;
    public bool hasRingSystem;
    public bool hasMagneticField;

    // additional info
    public string moreInfo;


    public CelestialData(GameObject model, Sprite im)
    {
        this.model = model;
        this.celestialImage = im;
    }

    public void SetDiameter(float diam)
    {
        this.diameter = diam;
        this.radius = diameter / 2;
    }
}