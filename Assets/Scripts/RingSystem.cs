using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingSystem : MonoBehaviour
{
    private float scaleMultiplier = 1;
    private float distanceMultiplier = 1;
    private float timeMultiplier = 1;
    public Transform baseCelestial;


    public GameController.Celestial celestialName;
    private CelestialData celestialData;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // check because it wasn't happening in Start()
        if (celestialData == null)
        {
            celestialData = GameController.celestials[celestialName];
            transform.Rotate(0, 0, celestialData.obliquityToOrbit); // z is the axis tilt
        }


        transform.localScale = Vector3.one * celestialData.modelRadius / 1000 * scaleMultiplier;
        transform.parent.localPosition = new Vector3(celestialData.modelDistanceFromSun, 0, 0) * distanceMultiplier;
    }

    public void ChangeScale(float val)
    {
        scaleMultiplier = val;
    }

    public void ChangeDistance(float val)
    {
        distanceMultiplier = val;
    }

    public void ChangeTime(float val)
    {
        timeMultiplier = val;
    }
}
