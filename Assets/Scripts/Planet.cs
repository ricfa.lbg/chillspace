using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    /*
    public float distancia_sol = 1;
    public float raio = 1;
    public float orbita = 1;
    public float rotacao = 1;


    private float velocidade_rotacao; 
    private float velocidade_translacao;
    public Transform base_planeta;
    */
    private float rotationSpeed;
    private float translactionSpeed;
    private float scaleMultiplier = 1;
    private float distanceMultiplier = 1;
    private float timeMultiplier = 1;
    public Transform baseCelestial;


    public GameController.Celestial celestialName;
    private CelestialData celestialData;

    //A função Start é chamada apenas no inicio quando o objecto é iniciado
    void Start()
    {
        /*
        ///transform.localScale = new Vector3(raio, raio, raio);
        transform.localScale = Vector3.one * raio/1000 * controlador.multiplicador_escala;
        transform.parent.localPosition = new Vector3(distancia_sol, 0,0) * controlador.multiplicador_distancia;
        */

        /// PROBLEMO - it actually enters here....making it pointless
        if(GameController.celestials.Count != 0)
        {
            celestialData = GameController.celestials[celestialName];
            transform.localScale = Vector3.one * celestialData.modelRadius / 1000 * scaleMultiplier;
            transform.parent.localPosition = new Vector3(celestialData.modelDistanceFromSun, 0, 0) * distanceMultiplier;
        }
    }

    // A função update é chamada todos os frames
    void Update()
    {
        /*
        transform.localScale = Vector3.one * raio/1000 * controlador.multiplicador_escala;
        transform.parent.localPosition = new Vector3(distancia_sol, 0,0) * controlador.multiplicador_distancia;


        //rotacao
        velocidade_rotacao = 1/rotacao * controlador.multiplicador_rotacao;
        transform.Rotate(0, velocidade_rotacao * Time.deltaTime ,  0);
        

        velocidade_translacao = 1/orbita * controlador.multiplicador_rotacao;
        if(base_planeta != null) { //verifica se existe a base_planeta
            base_planeta.Rotate(0, velocidade_translacao * Time.deltaTime, 0);
        }
        */

        // check because it wasn't happening in Start()
        if(celestialData == null)
        {
            celestialData = GameController.celestials[celestialName];
            transform.Rotate(0, 0, celestialData.obliquityToOrbit); // z is the axis tilt
        }

        transform.localScale = Vector3.one * celestialData.modelRadius / 1000 * scaleMultiplier;
        transform.parent.localPosition = new Vector3(celestialData.modelDistanceFromSun, 0, 0) * distanceMultiplier;

        //rotation
        rotationSpeed = 1 / celestialData.modelRotationPeriod * timeMultiplier;
        transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);


        translactionSpeed = 1 / celestialData.modelOrbitalPeriod * timeMultiplier;
        if (baseCelestial != null)
        {
            baseCelestial.Rotate(0, translactionSpeed * Time.deltaTime, 0);
        }

    }

    public void ChangeScale(float val)
    {
        scaleMultiplier = val;
    }

    public void ChangeDistance(float val)
    {
        distanceMultiplier = val;
    }

    public void ChangeTime(float val)
    {
        Debug.Log("entrou no change rotation do planet");
        timeMultiplier = val;
    }

}
