using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicPanelBehaviour : MonoBehaviour
{
    public AudioController controller;
    public GameObject playButton;
    public GameObject pauseButton;
    public GameObject nextButton;
    public GameObject prevButton;

    private RectTransform rect;
    [SerializeField]
    private CameraController cam;
    // Start is called before the first frame update
    void Start()
    {
        rect = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        // check if the mouse is inside the panel
        if (gameObject.activeInHierarchy)
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(
                 rect,
                 Input.mousePosition,
                 null))
            {
                cam.SetLookArround(false);
            }
            else
            {
                cam.SetLookArround(true);
            }
        }
    }

    public void PlayNext()
    {
        controller.PlayNext();
    }

    public void PlayPrev()
    {
        controller.PlayPrev();
    }

    public void Play()
    {
        controller.Play();
        playButton.SetActive(false);
        pauseButton.SetActive(true);
    }
    public void Pause()
    {
        controller.Pause();
        pauseButton.SetActive(false);
        playButton.SetActive(true);
    }

}
