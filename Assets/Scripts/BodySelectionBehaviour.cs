using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BodySelectionBehaviour : MonoBehaviour
{
    private RectTransform rect;

    // the order that will show up in the selection panel
    private List<GameController.Celestial> bodiesOrder = new List<GameController.Celestial>()
    {
        GameController.Celestial.Sun,
        GameController.Celestial.Mercury,
        GameController.Celestial.Venus,
        GameController.Celestial.Earth,
        GameController.Celestial.Moon,
        GameController.Celestial.Mars,
        GameController.Celestial.Phobos,
        GameController.Celestial.Deimos,
        GameController.Celestial.Jupiter,
        GameController.Celestial.Saturn,
        GameController.Celestial.Uranus,
        GameController.Celestial.Neptune
};

    public GameObject buttonBody1;
    public GameObject buttonBody2;
    public GameObject buttonBody3;
    public GameObject buttonBody4;

    private int currPos = 0;

    [SerializeField]
    private CameraController cam;
    public InfoPanelBehaviour info;
    // Start is called before the first frame update
    void Start()
    {

        rect = GetComponent<RectTransform>();
        currPos = 0;
        
        buttonBody1.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos]].celestialImage;
        buttonBody2.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos+1]].celestialImage;
        buttonBody3.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos+2]].celestialImage;
        buttonBody4.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos+3]].celestialImage;
        
    }

    // Update is called once per frame
    void Update()
    {
        // check if the mouse is inside the panel
        if (RectTransformUtility.RectangleContainsScreenPoint(
                rect,
                Input.mousePosition,
                null))
        {
            cam.SetLookArround(false);
        }
        else
        {
            cam.SetLookArround(true);
        }
    }

    public void SwapSpritesRight()
    {
        if( currPos < bodiesOrder.Count-4)
        {
            currPos += 1;
            buttonBody1.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos]].celestialImage;
            buttonBody2.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos + 1]].celestialImage;
            buttonBody3.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos + 2]].celestialImage;
            buttonBody4.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos + 3]].celestialImage;
        }
        else
        {
            // do nothing
        }
    }
    public void SwapSpritesLeft()
    {
        if (currPos >0)
        {
            currPos -= 1;
            buttonBody1.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos]].celestialImage;
            buttonBody2.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos + 1]].celestialImage;
            buttonBody3.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos + 2]].celestialImage;
            buttonBody4.GetComponent<Image>().sprite = GameController.celestials[bodiesOrder[currPos + 3]].celestialImage;
        }
        else
        {
            // do nothing
        }
    }

    public void LookAtCelestial(int btnNum)
    {
        switch(btnNum)
        {
            case 1: 
                cam.ChangeTarget(bodiesOrder[currPos]);
                info.ChangeTarget(bodiesOrder[currPos]);
                break;
            case 2:
                cam.ChangeTarget(bodiesOrder[currPos + 1]);
                info.ChangeTarget(bodiesOrder[currPos + 1]);
                break;
            case 3:
                cam.ChangeTarget(bodiesOrder[currPos + 2]);
                info.ChangeTarget(bodiesOrder[currPos + 2]);
                break;
            case 4:
                cam.ChangeTarget(bodiesOrder[currPos + 3]);
                info.ChangeTarget(bodiesOrder[currPos + 3]);
                break;
            default: 
                Debug.Log("unexpected @ LookAtCelestial");
                break;
        }
    }
}
