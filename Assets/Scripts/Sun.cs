using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{
    /*
    // public float raio = 696340;
    public float raio = 695700;
    public float multiplicador_sol = 0.008f;
    // public float multiplicador_sol = 1f;
    public SlidersBehaviour controlador;
    */

    public SlidersBehaviour slidersController;
    public GameController.Celestial celestialName;

    public float sunScaleMultiplier = 0.008f;
    // Start is called before the first frame update
    void Start()
    {
        celestialName = GameController.Celestial.Sun;

        /// PROBLEMO - it actually enters here....making it pointless
        if (GameController.celestials.Count != 0)
            transform.localScale = Vector3.one * GameController.celestials[celestialName].modelRadius/1000 * sunScaleMultiplier;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = Vector3.one * GameController.celestials[celestialName].modelRadius / 1000 * sunScaleMultiplier;
    }
}
