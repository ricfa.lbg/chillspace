using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SlidersBehaviour : MonoBehaviour
{
    public float scaleMultiplier = 0.001f;
    public float distanceMultiplier = 1;
    public float timeMultiplier = 20;

    public Slider SliderScale;
    public Slider SliderDistance;
    public Slider SliderTime;

    Planet[] planets;
    RingSystem[] rings;

    [SerializeField]
    private CameraController cam;

    private RectTransform rect;

    // public GameObject Sol;

    // Start is called before the first frame update
    void Start()
    {

        planets = FindObjectsOfType<Planet>();
        rings = FindObjectsOfType<RingSystem>();

        rect = gameObject.GetComponent<RectTransform>();
        //SliderEscala.onValueChanged.AddListener(delegate{MudaEscala();});
    }

    // Update is called once per frame
    void Update()
    {
        // check if the mouse is inside the panel
        if (gameObject.activeInHierarchy)
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(
                 rect,
                 Input.mousePosition,
                 null))
            {
                cam.SetLookArround(false);
            }
            else
            {
                cam.SetLookArround(true);
            }
        }
    }

    /*
    public void CriarSol() {
        GameObject.Instantiate<GameObject>(Sol, Vector3.zero, Quaternion.identity);
    }
    */
    public void ChangeScale() {
        scaleMultiplier = SliderScale.value;
        foreach(Planet planet in planets)
        {
            planet.ChangeScale(scaleMultiplier);
        }
        foreach (RingSystem ring in rings)
        {
            ring.ChangeScale(scaleMultiplier);
        }

    }

    public void ChangeDistance() {
        distanceMultiplier = SliderDistance.value;
        foreach (Planet planet in planets)
        {
            planet.ChangeDistance(distanceMultiplier);
        }
        foreach (RingSystem ring in rings)
        {
            ring.ChangeDistance(distanceMultiplier);
        }
    }

    public void ChangeRotation() {
        Debug.Log("entrou no change rotation do panel");
        timeMultiplier = SliderTime.value;
        foreach (Planet planet in planets)
        {
            planet.ChangeTime(timeMultiplier);
        }
        foreach (RingSystem ring in rings)
        {
            ring.ChangeTime(scaleMultiplier);
        }
    }

    public void QuitGame()
    {
        Debug.Log("quit!");
        Application.Quit();
    }
}