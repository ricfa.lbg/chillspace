using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlidersButtonBehaviour : MonoBehaviour
{

    // a reference to show and hide the slidersPanel
    public GameObject slidersPanel;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// Will toggle the panel with the sliders for the planets
    /// </summary>
    public void ToggleSliders()
    {
        slidersPanel.SetActive(!slidersPanel.activeInHierarchy);
    }

  

}
